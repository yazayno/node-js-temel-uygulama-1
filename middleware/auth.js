
//TODO BU sayfaya request öncesi bir arayazılım yazılarak request.isauthenticated değerleri eklenmesi sağlanacak

// https://stackoverflow.com/questions/47515991/how-to-setup-an-authentication-middleware-in-express-js
// https://openclassrooms.com/en/courses/5614116-go-full-stack-with-node-js-express-and-mongodb/5656301-set-up-authentication-middleware
const jsonwebtoken = require('jsonwebtoken');
const config = require("../config");
const userDb = require("../data/authDb");

module.exports.authenticationToken = async (req, res, next) => {
    try {

        const token = req.cookies.jwt;
        console.log("token->", token)
        if (token) {

            jsonwebtoken.verify(token, config.JWT_SECRET, async (err,authUser) => {

                const user = await userDb.getUserById(authUser.userId);
                req.user = user

            });
        }
        next();
    } catch (error) {
        console.log(error)
        next();
    }
}


