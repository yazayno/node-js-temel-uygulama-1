const mysql = require("mysql2");
const config = require("../config");
const pool = mysql.createPool(config.db);

let db = {};

db.allDistributors = () =>{
    return new Promise((resolve, reject)=>{
        pool.query('SELECT * FROM distributors ', (error, distributors)=>{
            if(error){
                return reject(error);
            }
            return resolve(distributors);
        });
    });
};

db.allMagazines = () =>{
    return new Promise((resolve, reject)=>{
        pool.query('SELECT * FROM magazines ', (error, magazines)=>{
            if(error){
                return reject(error);
            }
            return resolve(magazines);
        });
    });
};

db.allCustomers = () =>{
    return new Promise((resolve, reject)=>{
        pool.query('SELECT * FROM customers ', (error, customers)=>{
            if(error){
                return reject(error);
            }
            return resolve(customers);
        });
    });
};

db.getNewsBySlug = (slug) =>{
    return new Promise((resolve, reject)=>{
        pool.query('SELECT * FROM news WHERE slug = ?', [slug], (error, news)=>{
            if(error){
                return reject(error);
            }
            return resolve(news[0]);
        });
    });
};

db.allNews = () =>{
    return new Promise((resolve, reject)=>{
        pool.query('SELECT * FROM news ', (error, news)=>{
            if(error){
                return reject(error);
            }
            return resolve(news);
        });
    });
};


module.exports = db