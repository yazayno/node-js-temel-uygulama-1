const express = require("express");
const router = express.Router();
const cookieParser = require('cookie-parser');

var authController = require("../controllers/authController")

router.use(cookieParser());
router.post("/register", authController.register);
router.post("/login", authController.login);


module.exports = router;