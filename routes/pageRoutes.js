const express = require("express");
const router = express.Router();
var pageController = require("../controllers/pageController")
var authMiddleware = require("../middleware/auth")

router.get("/news/:slug",authMiddleware.authenticationToken, pageController.newsDetail);
router.get("/",authMiddleware.authenticationToken, pageController.homePage);
router.get("/redirectExample", pageController.redirectExample);
router.get("/login",pageController.loginPage);

module.exports = router;