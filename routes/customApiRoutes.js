const express = require("express");
const router = express.Router();
var customApiController = require("../controllers/customApiController")

router.get("/news/", customApiController.getNewsData);
router.post("/login/", customApiController.login);


module.exports = router;