const express = require("express");
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const pageRoutes = require("./routes/pageRoutes");
const authRoutes = require("./routes/authRoutes");
const customApiRoutes = require("./routes/customApiRoutes");

app.set("view engine", "ejs");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(express.static('public'));
app.use(express.static('node_modules'));

app.use(cors());


app.use(cookieParser());

app.use('/',pageRoutes);
app.use('/auth',authRoutes);
app.use('/api',customApiRoutes);

app.get('*', function(req, res){
    res.render("404");
});

app.listen(3000, () => {
    console.log("listening on port 3000");
});

