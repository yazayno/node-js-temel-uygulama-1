const userDb = require("../data/authDb");
const config = require("../config")
const loginUrl = config.loginUrl

const jsonwebtoken = require('jsonwebtoken');
const { hashSync, compareSync } = require("bcrypt");


module.exports.register = async (req, res, next) => {
    try {
        const userName = req.body.userName;
        const email = req.body.email;
        let password = req.body.password;

        console.log(userName,email,password)


        if (!userName || !email || !password) {
            return res.sendStatus(400);
        }

        password = hashSync(password, 10);

        const user = await userDb.insertUser(userName, email, password);

        const jsontoken = jsonwebtoken.sign({ user: user }, config.secretKey, { expiresIn: '30m' });
        res.cookie('token', jsontoken, { httpOnly: true, secure: true, SameSite: 'strict', expires: new Date(Number(new Date()) + 30 * 60 * 1000) }); //we add secure: true, when using https.


        res.json({ token: jsontoken });

        //return res.redirect('/mainpage');

    } catch (e) {
        console.log(e);
        res.sendStatus(400);
    }
}

module.exports.login = async (req, res, next) => {
    try {
        const email = req.body.email;
        const password = req.body.password;

        console.log(email,password)

        user = await userDb.getUserByEmail(email);

        if (!user) {
            return res.json({
                message: "Invalid email or password"
            })
        }

        const isValidPassword = compareSync(password, user.password);
        if (isValidPassword) {
            user.password = undefined;
            const jsontoken = jsonwebtoken.sign({ user: user }, config.secretKey, { expiresIn: '30m' });
            res.cookie('token', jsontoken, { httpOnly: true, secure: true, SameSite: 'strict', expires: new Date(Number(new Date()) + 30 * 60 * 1000) }); 
            res.json({ token: jsontoken });

        } else {
            return res.json({
                message: "Invalid email or password"
            });
        }

    } catch (e) {
        console.log(e);
    }
}