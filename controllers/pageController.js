const pageDb = require("../data/pageDb");
const config = require("../config")
const loginUrl = config.loginUrl

module.exports.newsDetail = async function (req, res) {
    try {
        const distributors = await pageDb.allDistributors();
        const magazines = await pageDb.allMagazines();
        const customers = await pageDb.allCustomers();
        const news = await pageDb.getNewsBySlug(req.params.slug);
        res.render("news/details", {
            distributors: distributors,
            magazines: magazines,
            customers: customers,
            news: news,
            req: req
        });
    }
    catch (err) {
        console.log(err);
    }
}

module.exports.homePage = async function (req, res) {
    try {

    
        const distributors = await pageDb.allDistributors();
        const magazines = await pageDb.allMagazines();
        const customers = await pageDb.allCustomers();
        const news = await pageDb.allNews();

        res.render("homepage/index", {
            distributors: distributors,
            magazines: magazines,
            customers: customers,
            news: news,
            req: req
        });
    }
    catch (err) {
        console.log(err);
    }
}

module.exports.redirectExample = async function (req, res) {
    res.redirect(loginUrl)
}


module.exports.loginPage = async function (req, res) {

    res.render("auth/login", {
        req: req
    });

}