const pageDb = require("../data/pageDb");
const authDb = require("../data/authDb");
const config = require("../config");

const jsonwebtoken = require('jsonwebtoken');
const { hashSync, genSaltSync, compareSync } = require("bcrypt");

module.exports.getNewsData = async function (req, res) {
    try {

        console.log("req.isAuthenticated", req.isAuthenticated)

        const news = await pageDb.allNews();
        res.json({ status: true, datas: news });
    }
    catch (err) {
        res.json({ status: false, error_message: String(err) });
    }
}

const createToken = (userId) => {
    return jsonwebtoken.sign({ userId }, config.JWT_SECRET, {
      expiresIn: '1d',
    });
  };

module.exports.login = async function (req, res) {
    try {
        const { username, password } = req.body;
        var error_messages = []
        const user = await authDb.getUserByUsername(username)

        let same = false;

        if (user) {
            console.log("user.password",user.password)
            same = await compareSync(password, user.password);
        } else {
            error_messages.push('There is no such user')
        }

        if (same) {
            console.log("same",same)
            const token = createToken(user.id);
            
            res.cookie('jwt', token, {
                httpOnly: true,
                maxAge: 1000 * 60 * 60 * 24,
            });

            res.redirect('/');  
            
        } else {
            error_messages.push('Paswords are not matched')
        }

        console.log(error_messages)

    } catch (error) {
        error_messages.push(String(error))
    }
}